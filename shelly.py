from app.libraries.database import Database
from app.libraries.shellydevice import Shelly
from datetime import datetime, date, timedelta
import time
import sys
import os
import requests
import pdb

if __name__ == '__main__':

    db = Database()

    obj = Shelly()

    data = obj.status()

    now = datetime.now()

    if isinstance(data, dict) and data['em1:0']['act_power'] != None:

        db.save_a(now, data['em1:0']['current'], data['em1:0']['voltage'], data['em1:0']['act_power'], data['em1:0']['aprt_power'], data['em1:0']['pf'])

        db.save_b(now, data['em1:1']['current'], data['em1:1']['voltage'], data['em1:1']['act_power'], data['em1:1']['aprt_power'], data['em1:1']['pf'])

        db.save_c(now, data['em1:2']['current'], data['em1:2']['voltage'], data['em1:2']['act_power'], data['em1:2']['aprt_power'], data['em1:2']['pf'])

        db.save_temp(now, data['temperature:0']['tC'])

        energy_a = data['em1data:0']['total_act_energy'],
        energy_b = data['em1data:1']['total_act_energy'],
        energy_c = data['em1data:2']['total_act_energy'],

        total_energy = energy_a[0] + energy_b[0] + energy_c[0]

        db.save_totals(
                now,
                energy_a[0],
                energy_b[0],
                energy_c[0],
                total_energy
                )

        lastday = db.lastday_check()

        if not lastday:

            last_day_act_power = db.lastday()

            db.lastday_save(date.today() - timedelta(days=1), last_day_act_power)

        lastmonth = db.lastmonth_check()

        last_month_act_power = db.lastmonth()

        if last_month_act_power == None:

            last_month_act_power = 0

        db.lastmonth_save(now.date(), now.year, now.month, last_month_act_power)

    else:

        response = obj.reboot()

        if response.ok:

            db.reboot_save(now, True)

            print("Rebooting device...")
