# shelly
Obté les dades d'un dispositiu Shelly Pro 3EM, les desa en una base de dades Postgresql per alimentar com a font de dades a Grafana.

### Dependències

-   **Python 3**
-   Servidor Postgresql
-   Domini en un servei DDNS que apunti a la IP dinàmica de casa

### Com instal·lar shelly:

1. Clona el repositori: `git clone https://codeberg.org/spla/shelly.git <directori>`  

2. Entra amb `cd` al directori `<directori>` i crea l'Entorn Virtual de Python: `python3.x -m venv .`  
 
3. Activa l'Entorn Virtual de Python: `source bin/activate`  
  
4. Executa `pip install -r requirements.txt` per a afegir les llibreries necessàries.  

5. Executa `python shelly.py`. La primera execució genera la configuració inicial i crea la base de dades.  

Aquest és el resultat a Grafana ![resultat visual a Grafana](app/imatges/shelly.png "corbes de consum a Grafana")

18.3.2024 *novetat* Classe Shelly creada, inclou els mètodes list_methods(Shelly.Listmethods), reboot(Shelly.Reboot) i status(Shelly.GetStatus).  
21.3.2024 *novetat* Canvi del perfil trifàsic a monofàsic.  
22.3.2024 *novetat* Afegida lectura de la temperatura del dispositiu Shelly Pro 3EM.   
23.3.2024 *novetat* Si cal reiniciar el dispositiu, es desa quan s'ha fet en la nova base de dades 'reboots'.

