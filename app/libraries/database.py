import os
import sys
import random
import psycopg2
from psycopg2 import sql
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from getpass import getpass
import pdb

class Database():

    name = 'database library'

    def __init__(self, config_file=None, db=None, db_user=None, db_user_password=None):

        self.config_file = "config/db_config.txt"
        self.db = self.__get_parameter("db", self.config_file)
        self.db_user = self.__get_parameter("db_user", self.config_file)
        self.db_user_password = self.__get_parameter("db_user_password", self.config_file)
        self.domain = self.__get_parameter("domain", self.config_file)

        db_setup = self.__check_dbsetup(self)

        if not db_setup:

            self.db = input("\ndatabase name: ")
            self.db_user = input("\ndatabase user: ")
            self.db_user_password = getpass("\ndatabase user password: ")
            self.domain = input("\nShelly domain: ")

            self.__createdb(self)  
            self.__create_config(self)
            self.__write_config(self)

    def save_a(self, datetime, current, voltage, act_power, aprt_power, pf):

        insert_sql = "INSERT INTO cuina(checked_at, current, voltage, act_power, aprt_power, pf) VALUES(%s,%s,%s,%s,%s,%s) ON CONFLICT DO NOTHING"

        conn = None

        try:

            conn = psycopg2.connect(database=self.db, user=self.db_user, password="", host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(insert_sql, (datetime, current, voltage, act_power, aprt_power, pf))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def save_b(self, datetime,  current, voltage, act_power, aprt_power, pf):

        insert_sql = "INSERT INTO resta_pis(checked_at, current, voltage, act_power, aprt_power, pf) VALUES(%s,%s,%s,%s,%s,%s) ON CONFLICT DO NOTHING"

        conn = None

        try:

            conn = psycopg2.connect(database=self.db, user=self.db_user, password="", host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(insert_sql, (datetime, current, voltage, act_power, aprt_power, pf))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def save_c(self, datetime, current, voltage, act_power, aprt_power, pf):

        insert_sql = "INSERT INTO vitro(checked_at, current, voltage, act_power, aprt_power, pf) VALUES(%s,%s,%s,%s,%s,%s) ON CONFLICT DO NOTHING"

        conn = None

        try:

            conn = psycopg2.connect(database=self.db, user=self.db_user, password="", host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(insert_sql, (datetime, current, voltage, act_power, aprt_power, pf))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def save_temp(self, datetime, temp):

        insert_sql = "INSERT INTO temp(checked_at, temperature) VALUES(%s,%s) ON CONFLICT DO NOTHING"

        conn = None

        try:

            conn = psycopg2.connect(database=self.db, user=self.db_user, password="", host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(insert_sql, (datetime, temp))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def save_totals(self, datetime, a_total, b_total, c_total, total):

        insert_sql = "INSERT INTO totals(checked_at, a_total, b_total, c_total, total) VALUES(%s,%s,%s,%s,%s) ON CONFLICT DO NOTHING"

        conn = None

        try:

            conn = psycopg2.connect(database=self.db, user=self.db_user, password="", host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(insert_sql, (datetime, a_total, b_total, c_total, total))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def lastday_check(self):

        found = False

        sql = "select lastday_total from daily where date_part('month', checked_at) = date_part('month', now()) and date_part('day', checked_at)= date_part('day', now() - interval '1 day')"

        conn = None

        try:

            conn = psycopg2.connect(database=self.db, user=self.db_user, password="", host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(sql)

            row = cur.fetchone()

            if row != None:

                found = True

            cur.close()

            return found

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def lastday(self):

        sql2 = "select total from totals where date_part('day', checked_at)= date_part('day', now() - interval '2 day') order by checked_at desc limit 1"

        sql1 = "select total from totals where date_part('day', checked_at)= date_part('day', now() - interval '1 day') order by checked_at desc limit 1"

        conn = None

        try:

            conn = psycopg2.connect(database=self.db, user=self.db_user, password="", host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(sql2)

            row = cur.fetchone()

            if row != None:

                two_days_act_power = row[0]

            cur.execute(sql1)

            row = cur.fetchone()

            if row != None:

                lastday_act_power = row[0]

            cur.close()

            return lastday_act_power - two_days_act_power 

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def lastday_save(self, date, lastday_total):

        insert_sql = "INSERT INTO daily(checked_at, lastday_total) VALUES(%s,%s) ON CONFLICT DO NOTHING"

        conn = None

        try:

            conn = psycopg2.connect(database=self.db, user=self.db_user, password="", host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(insert_sql, (date, lastday_total))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def lastmonth_check(self):

        found = False

        sql = "select lastmonth_total from monthly where month = date_part('month', now()) and year = date_part('year', now())"

        conn = None

        try:

            conn = psycopg2.connect(database=self.db, user=self.db_user, password="", host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(sql)

            row = cur.fetchone()

            if row != None:

                found = True

            cur.close()

            return found

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def lastmonth(self):

        last_month_act_power = 0

        sql = "select sum(lastday_total) from daily where date_part('month', checked_at) = date_part('month', now())"

        conn = None

        try:

            conn = psycopg2.connect(database=self.db, user=self.db_user, password="", host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(sql)

            row = cur.fetchone()

            if row != None:

                last_month_act_power = row[0]

            cur.close()

            return last_month_act_power

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def lastmonth_save(self, date, year, month, lastmonth_total):

        insert_sql = "INSERT INTO monthly(checked_at, year, month, lastmonth_total) VALUES(%s,%s,%s,%s) ON CONFLICT DO NOTHING"

        update_sql = "UPDATE monthly SET checked_at=(%s), lastmonth_total=(%s) where month=(%s) and year=(%s)"

        conn = None

        try:

            conn = psycopg2.connect(database=self.db, user=self.db_user, password="", host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(insert_sql, (date, year, month, lastmonth_total))

            cur.execute(update_sql, (date, lastmonth_total, month, year))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def reboot_save(self, date, reboot):

        insert_sql = "INSERT INTO reboots(checked_at, reboot) VALUES(%s,%s) ON CONFLICT DO NOTHING"

        conn = None

        try:

            conn = psycopg2.connect(database=self.db, user=self.db_user, password="", host="/var/run/postgresql", port="5432")

            cur = conn.cursor()

            cur.execute(insert_sql, (date, reboot))

            conn.commit()

            cur.close()

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    @staticmethod
    def __check_dbsetup(self):

        db_setup = False

        try:

            conn = None

            conn = psycopg2.connect(database = self.db, user = self.db_user, password = self.db_user_password, host = "/var/run/postgresql", port = "5432")

            db_setup = True

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        return db_setup

    @staticmethod
    def __createdb(self):

        conn = None

        try:

            conn = psycopg2.connect(dbname='postgres',
                user=self.db_user, host='',
                password=self.db_user_password)

            conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

            cur = conn.cursor()

            print(f"Creating database {self.db}. Please wait...")

            cur.execute(sql.SQL("CREATE DATABASE {}").format(
                sql.Identifier(self.db))
            )
            print(f"Database {self.db} created!\n")

            self.__dbtables_schemes(self)

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    @staticmethod
    def __dbtables_schemes(self):

        table = "cuina"
        sql = f"create table {table} (checked_at timestamptz, current float, voltage float, act_power float, aprt_power float, pf float)"
        self.__create_table(self, table, sql)

        table = "resta_pis"
        sql = f"create table {table} (checked_at timestamptz, current float, voltage float, act_power float, aprt_power float, pf float)"
        self.__create_table(self, table, sql)

        table = "vitro"
        sql = f"create table {table} (checked_at timestamptz, current float, voltage float, act_power float, aprt_power float, pf float)"
        self.__create_table(self, table, sql)

        table = "temp"
        sql = f"create table {table} (checked_at timestamptz, temperature float)"
        self.__create_table(self, table, sql)

        table = "totals"
        sql = f"create table {table} (checked_at timestamptz, a_total float, b_total float, c_total float, total float)"
        self.__create_table(self, table, sql)

        table = "daily"
        sql = f"create table {table} (checked_at date, lastday_total float)"
        self.__create_table(self, table, sql)

        table = "monthly"
        sql = f"create table {table} (checked_at date PRIMARY KEY, month int, lastmonth_total float)"
        self.__create_table(self, table, sql)

        index = "monthly_pkey"
        sql_index = f'CREATE UNIQUE INDEX {index} ON {table} (year, month)'
        self.__create_index(self, table, index, sql_index)

        table = "reboots"
        sql = f"create table {table} (checked_at date, reset boolean)"
        self.__create_table(self, table, sql)

    @staticmethod
    def __create_table(self, table, sql):

        conn = None

        try:

            conn = psycopg2.connect(database = self.db, user = self.db_user, password = self.db_user_password, host = "/var/run/postgresql", port = "5432")

            cur = conn.cursor()

            print(f"Creating table {table}")

            cur.execute(sql)

            conn.commit()

            print(f"Table {table} created!\n")

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    @staticmethod
    def __create_index(self, table, index, sql_index):

        conn = None

        try:

            conn = psycopg2.connect(database = self.db, user = self.db_user, password = "", host = "/var/run/postgresql", port = "5432")
            cur = conn.cursor()

            print(f"Creating index...{index}")
            # Create the table in PostgreSQL database
            cur.execute(sql_index)

            conn.commit()

            print(f"Index {index} created!\n")

        except (Exception, psycopg2.DatabaseError) as error:

            print(error)

        finally:

            if conn is not None:

                conn.close()

    def __get_parameter(self, parameter, config_file):

        if not os.path.isfile(config_file):
            print(f"File {config_file} not found..")
            return

        with open( config_file ) as f:
            for line in f:
                if line.startswith( parameter ):
                    return line.replace(parameter + ":", "").strip()

        print(f"{config_file} Missing parameter {parameter}")

        sys.exit(0)

    @staticmethod
    def __create_config(self):

        if not os.path.exists('config'):

            os.makedirs('config')

        if not os.path.exists(self.config_file):

            print(self.config_file + " created!")
            with open(self.config_file, 'w'): pass

    @staticmethod
    def __write_config(self):

        with open(self.config_file, 'a') as the_file:

            the_file.write(f'db: {self.db}\ndb_user: {self.db_user}\ndb_user_password: {self.db_user_password}\ndomain: {self.domain}')

            print(f"adding parameters to {self.config_file}\n")


