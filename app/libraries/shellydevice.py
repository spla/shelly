from app.libraries.database import Database
import sys
import os
import requests
import pdb

###
# Dict helper class.
# Defined at top level so it can be pickled.
###
class AttribAccessDict(dict):
    def __getattr__(self, attr):
        if attr in self:
            return self[attr]
        else:
            raise AttributeError("Attribute not found: " + str(attr))

    def __setattr__(self, attr, val):
        if attr in self:
            raise AttributeError("Attribute-style access is read only")
        super(AttribAccessDict, self).__setattr__(attr, val)

class Shelly:

    name = 'Shelly Pro 3EM class'

    def __init__(self, domain=None, shelly_device_url=None, session=None):

        db = Database()

        self.domain = db.domain

        self.shelly_base_url = f'http://{self.domain}/rpc'

        if session:
            self.session = session
        else:
            self.session = requests.Session()

    def list_methods(self):

        endpoint = self.shelly_base_url + '/Shelly.Listmethods'

        response = self.__rpc_request('GET', endpoint)

        response = self.__json_allow_dict_attrs(response.json())

        return (response)

    def reboot(self):

        endpoint = self.shelly_base_url + '/Shelly.Reboot'

        response = self.__rpc_request('GET', endpoint)

        return (response)

    def status(self):

        endpoint = self.shelly_base_url + '/Shelly.GetStatus'

        response = self.__rpc_request('GET', endpoint)

        response = self.__json_allow_dict_attrs(response.json())

        return (response)

    def __rpc_request(self, method, endpoint, data={}):

        response = None

        try:

            response = self.session.request(method, url = endpoint)

        except Exception as e:

            raise ShellyNetworkError(f"Could not complete request: {e}")

        if response is None:

            raise ShellyIllegalArgumentError("Illegal request.")

        if not response.ok:

                try:
                    if isinstance(response, dict) and 'error' in response:
                        error_msg = response['error']
                    elif isinstance(response, str):
                        error_msg = response
                    else:
                        error_msg = None
                except ValueError:
                    error_msg = None

                if response.status_code == 404:
                    ex_type = ShellyNotFoundError
                    if not error_msg:
                        error_msg = 'Endpoint not found.'
                        # this is for compatibility with older versions
                        # which raised ShellyAPIError('Endpoint not found.')
                        # on any 404
                elif response.status_code == 401:
                    ex_type = ShellyUnauthorizedError
                elif response.status_code == 422:
                    return response
                elif response.status_code == 500:
                    ex_type = ShellyInternalServerError
                elif response.status_code == 502:
                    ex_type = ShellyBadGatewayError
                elif response.status_code == 503:
                    ex_type = ShellyServiceUnavailableError
                elif response.status_code == 504:
                    ex_type = ShellyGatewayTimeoutError
                elif response.status_code >= 500 and \
                    response.status_code <= 511:
                    ex_type = ShellyServerError
                else:
                    ex_type = ShellyAPIError

                raise ex_type(
                    'Shelly API returned error',
                    response.status_code,
                    response.reason,
                    error_msg)

        else:

            return response

    @staticmethod
    def __json_allow_dict_attrs(json_object):
        """
        Makes it possible to use attribute notation to access a dicts
        elements, while still allowing the dict to act as a dict.
        """
        if isinstance(json_object, dict):
            return AttribAccessDict(json_object)
        return json_objecte
    ##

# Exceptions
##
class ShellyError(Exception):
    """Base class for shellydevice.py exceptions"""

class ShellyIOError(IOError, ShellyError):
    """Base class for shellydevice.py I/O errors"""

class ShellyNetworkError(ShellyIOError):
    """Raised when network communication with the server fails"""
    pass
class ShellyAPIError(ShellyError):
    """Raised when the shelly API generates a response that cannot be handled"""
    pass
class ShellyServerError(ShellyAPIError):
    """Raised if the Server is malconfigured and returns a 5xx error code"""
    pass
class ShellyInternalServerError(ShellyServerError):
    """Raised if the Server returns a 500 error"""
    pass

class ShellyBadGatewayError(ShellyServerError):
    """Raised if the Server returns a 502 error"""
    pass

class ShellyServiceUnavailableError(ShellyServerError):
    """Raised if the Server returns a 503 error"""
    pass
class ShellyGatewayTimeoutError(ShellyServerError):
    """Raised if the Server returns a 504 error"""
    pass
class ShellyNotFoundError(ShellyAPIError):
    """Raised when the shelly API returns a 404 Not Found error"""
    pass

class ShellyUnauthorizedError(ShellyAPIError):
    """Raised when the shelly API returns a 401 Unauthorized error

       This happens when an OAuth token is invalid or has been revoked,
       or when trying to access an endpoint that can't be used without
       authentication without providing credentials."""
    pass
